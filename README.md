# Chess #

This project was created as a way for me to gain some experience coding in c++ and implement something complicated and enjoyful at the same time.
All the required rules have been followed in this implementation of chess, including en Passant and castling. However currently pawns automatically 
promote to a queen piece when reaching the promotion rank. 

The opponent is the stockfish engine which plays the black pieces and which follows the uci protocol for chess, and you get to verse the engine as white.
	
### Board  ###
![chess board](graphics/demo/chess.PNG chess board)

### Demo Game Play  ###
![sample game play](graphics/demo/sample_game.gif sample gameplay)

### Checkmate  ###
![checkmate](graphics/demo/checkmate.PNG checkmate)

### Tools used ###

* Visual Studio 2019
* Windows SDK version 10.0
* c++ 17

#### notes ####
* @author Zane Keyan
* note the project has still to be marked as fully complete and I am adding to it in my sparetime.
