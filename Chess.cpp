#include <iostream>
#include "SFML/Graphics.hpp"
#include "CreateChessBoardAndSets.h"
#include "Piece.h"
#include "Knight.cpp"
#include "Bishop.cpp"
#include "Rook.cpp"
#include "Queen.cpp"
#include "Pawn.cpp"
#include "GameManager.h"

using namespace sf;

/*
* @author zane keyan
*/
int main() {
	// create video mode object
	VideoMode vm(BOARD_SIZE_PIXEL, BOARD_SIZE_PIXEL);

	// create and open game window
	RenderWindow window(vm, "Chess");

	// init and create backgroud
	VertexArray background = createChessBoardBackground();

	//init currently selected piece fields
	VertexArray possibleMovesSelectionVA;
	VertexArray moveTracerVA;
	VertexArray kingCheckVA;


	shared_ptr<Piece> selectedPiece = nullptr;
	bool pieceSelected = false;
	bool inCheck = false;
	bool isCheckMate = false;
	tuple<Position*, bool, bool> checkOrCheckmateTuple;

	//instantiate gameManager
	GameManager gameManager;

	//load font
	sf::Font font;
	font.loadFromFile("fonts/VeraMono.ttf");

	// get white and black pieces
	vector<shared_ptr<Piece>> whitePiecesSet = gameManager.getWhiteSet();
	vector<shared_ptr<Piece>> blackPiecesSet = gameManager.getBlackSet();

	//default to whiteSet
	vector<shared_ptr<Piece>> currentSet = whitePiecesSet;

	//deteremine  if its black or whites turn to move based on even and odd number that iterates after each move
	int whiteOrBlackTurn = 0;

	//track mouse position
	Vector2f mousePosition;

	// keep track of game state
	string currentSetToMove;
	string whiteMoveInChessNotation;
	string blackMove;
	sf::Event event;

	// run
	while (window.isOpen()) {
		kingCheckVA.clear();
		//terminate game if ESC key is pressed
		if (Keyboard::isKeyPressed(Keyboard::Escape)) window.close();
		
		//determine who's turn it currently is to play
		if (whiteOrBlackTurn % 2 == 0) {
			currentSet = whitePiecesSet;
			currentSetToMove = WHITE_SIDE;
		} else {
			currentSet = blackPiecesSet;
			currentSetToMove = BLACK_SIDE;
		}
		
		// determine if check or checkmate occurs
		checkOrCheckmateTuple = gameManager.checkForCheckOrCheckmate(currentSetToMove);
		


		// black turn to move
		if (whiteOrBlackTurn % 2 != 0) {
			tuple<Position, Position, string> returnedTuple = gameManager.getEngineMoveForBlack(whiteMoveInChessNotation);
			Position oldPosition = get<0>(returnedTuple);
			Position newPosition = get<1>(returnedTuple);
			blackMove = get<2>(returnedTuple);
			moveTracerVA = drawMoveTracer(oldPosition, newPosition);
			sf::sleep(sf::seconds(.5f));

			//check for pawn promotion
			gameManager.checkForPawnPromotion(currentSetToMove);
				

			//update
			whiteMoveInChessNotation += +" " + blackMove + " ";
			blackPiecesSet = gameManager.getBlackSet();
			whitePiecesSet = gameManager.getWhiteSet();
			whiteOrBlackTurn++;
		}

		//white turn to move
		while (window.pollEvent(event))
		{
			//get mouse position
			mousePosition = window.mapPixelToCoords(sf::Mouse::getPosition(window));
			
			// handle click
			if (event.type == sf::Event::MouseButtonPressed && !pieceSelected)
			{
				// if a piece from the current playing set was clicked
				if (!pieceSelected) {
					for (int y = 0; y < currentSet.size(); y++) {

						Piece currentPiece = *currentSet.at(y);
						if (currentPiece.getSprite().getGlobalBounds().contains(mousePosition)) {
							possibleMovesSelectionVA = drawPossibleMovesOnBoard(gameManager.getValidMovesForPiece(currentSet.at(y)));
							pieceSelected = true;
							selectedPiece = currentSet.at(y);
						}
					}
				} 
			} 
			// else if click was on a tile , determine if click was on a possible move of the currently selected piece or some random empty tile
			else if( event.type == sf::Event::MouseButtonPressed && pieceSelected) {

				// if click was on a possible move for the selected piece , move to the selected tile
				for (auto move : gameManager.getValidMovesForPiece(selectedPiece)) {

					bool withinXRange = mousePosition.x >= move.getXSpritePosition() && mousePosition.x < (move.getXSpritePosition() + TILE_SIZE);
					bool withinYRange = mousePosition.y >= move.getYSpritePosition() && mousePosition.y < (move.getYSpritePosition() + TILE_SIZE);

					if (withinXRange & withinYRange) {
						whiteMoveInChessNotation += gameManager.translateMoveToChessNotation(selectedPiece, move);
						
						moveTracerVA = drawMoveTracer(selectedPiece->getPosition(), move);
						gameManager.setPositionOfPiece(selectedPiece , move);

						gameManager.checkForPawnPromotion(currentSetToMove);

						//update
						whitePiecesSet = gameManager.getWhiteSet();
						blackPiecesSet = gameManager.getBlackSet();
						whiteOrBlackTurn++;
						break;
					}
				}
				
				//reset
				pieceSelected = false;
			}	
		}

		// clear window
		window.clear();

		// check if king is in check or checkmate has occured
		


		inCheck = get<1>(checkOrCheckmateTuple);
		if (inCheck) {
			Position* kingPos = get<0>(checkOrCheckmateTuple);
			kingCheckVA = drawCheck(*kingPos);
		}

	

		//draw background
		window.draw(background);
		window.draw(moveTracerVA);
		window.draw(kingCheckVA);

		// draw possible moves for the selected piece if required
		if (pieceSelected) window.draw(possibleMovesSelectionVA);

		// draw white and black set pieces
		for (int y = 0; y < whitePiecesSet.size(); y++) window.draw(whitePiecesSet.at(y)->getSprite());
		for (int y = 0; y < blackPiecesSet.size(); y++) window.draw(blackPiecesSet.at(y)->getSprite());

		isCheckMate = get<2>(checkOrCheckmateTuple);
		if (isCheckMate) {
			cout << gameManager.getOpponentSide(currentSetToMove) << " has won the game" << endl;
			string victoriousSide = gameManager.getOpponentSide(currentSetToMove);

			window.draw(getResultRectangle());
			window.draw(getResultText(victoriousSide, font));
			window.display();
			sleep(sf::seconds(5.0f));
			window.close();
		};
		//display
		window.display();

	}

	gameManager.closeEngine();

	return 0;
}

