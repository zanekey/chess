#pragma once
#include "SFML/Graphics.hpp";
#include <vector>
#include "Position.h"
#include "Piece.h"

/*
* @author zane keyan
*/

using namespace sf;
/*
* draw a the background of a chess board
*/

extern VertexArray createChessBoardBackground();
/*
* highlight the king tile if it is under attack
*
* @param position : position of the king to be highlighted
*
* @return vertexArray
*/
extern VertexArray drawPossibleMovesOnBoard(std::vector<Position> possibleMoves);

/*
* when a piece moves , highlight the previous position with one color and the new position with a new colour for a better user experience
*
* @param oldPosition
* @param newPosition
*
* @return vertexArray
*/
extern vector<shared_ptr<Piece>> getWhiteSetPieces();

/* draws the possibles moves of a piece on the GUI
*
* @param selectedPiecePossibleMoves : collection of the moves possible for a piece
*
* @return possibleMoves : vertexArray with the graphical representation of the moves possible
*/
extern vector<shared_ptr<Piece>>  getBlackSetPieces();

/* Init black set pieces to their appropiate positions
*
* return whiteSetPieces: vector collection of a complete white set
*/
VertexArray drawMoveTracer(Position oldPosition, Position newPosition);

/* Init black set pieces to their appropiate positions
*
* return blackSetPieces: vector collection of a complete black set
*/
VertexArray drawCheck(Position position);

/*
* rectangle to be displayed when match has ended
*
* @return resultRectangle
*/
RectangleShape getResultRectangle();

/*
* Result to be drawn over resultRectangle when match has concluded
*
* @param victoriousSide
* @param font
*
* @return resultText
*/
Text getResultText(string victoriousSide , Font &font);