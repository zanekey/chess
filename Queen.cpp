#include "Piece.h"

class Queen : public Piece {


public:
	Queen(Position initPosition, string side) : Piece(initPosition, side) {

		pieceName = side + QUEEN;
		imgFile = QUEEN_PIECE_IMG_FILE;
		Piece::setSpriteSettings();
		limitedMovement = false;
		xMoveOffsets = { 1, 1, -1, -1 , 0 , 1 , 0 , -1 };
		yMoveOffsets = { 1, -1 , -1 , 1 , 1 , 0 , -1 , 0 };

	}
};