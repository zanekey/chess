#include "Piece.h"

class Pawn : public Piece {

private:
	bool hasMadeFirstMove = false;
	vector<int> attackCoordsX;
	vector<int> attackCoordsY;


public:
	Pawn(Position initPosition, string side) : Piece(initPosition, side) {
		
		imgFile = PAWN_PIECE_IMG_FILE;
		limitedMovement = true;
		pieceName = side + PAWN;
		Piece::setSpriteSettings();
		
		if (side == WHITE_SIDE){
			xMoveOffsets = { 0 , 0 };
			yMoveOffsets = { -1  , -2 };
			attackCoordsX = { 1 , -1 };
			attackCoordsY = { -1 , -1 };
		}
		else
		{
			xMoveOffsets = { 0 , 0 };
			yMoveOffsets = { 1 , 2 };
			attackCoordsX = { 1 , -1 };
			attackCoordsY = { 1 , 1 };
		}
	}


	void setPosition(Position position) {

		// ensure pawn can only move one step after first move
		if (!hasMadeFirstMove) {
			xMoveOffsets.pop_back();
			yMoveOffsets.pop_back();
			hasMadeFirstMove = true;
		}

		Piece::setPosition(position);

	}

	vector<int> getAttackCoordsX() { return attackCoordsX;  }
	vector<int> getAttackCoordsY() { return attackCoordsY;  }

};