#pragma once
#include "DEFINITIONS.h"

class Position {

private:
	int xPosition;
	int yPosition;
	float xSpritePosition;
	float ySpritePosition;

public:
	Position(){};
	Position(int x, int y) : xPosition(x), yPosition(y) , xSpritePosition(float (x * TILE_SIZE)) , ySpritePosition( float (y * TILE_SIZE)) {};
	int getXPosition() { return xPosition;  }
	int getYPosition() { return yPosition;  }
	float  getXSpritePosition() { return xSpritePosition; }
	float getYSpritePosition() { return ySpritePosition; }
	void setXPosition(int x) { xPosition = x ;  xSpritePosition = float (x * TILE_SIZE); }
	void setYPosition(int y) { yPosition = y;  ySpritePosition =  float (y * TILE_SIZE); }


};