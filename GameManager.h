#pragma once
#include <list>
#include "Piece.h"
#include "CreateChessBoardAndSets.h"
#include <iostream>
#include "King.cpp"

/*
* @author zane keyan
*/
class GameManager {

private:
	shared_ptr<Piece> enPassantPawn = nullptr;
protected:
	vector<shared_ptr<Piece>> whiteSetPieces;
	vector<shared_ptr<Piece>> blackSetPieces;
	std::string boardReference[BOARD_DIMENSION][BOARD_DIMENSION];

public:
	GameManager();

	vector<shared_ptr<Piece>> getWhiteSet() { return whiteSetPieces; }
	vector<shared_ptr<Piece>> getBlackSet() { return blackSetPieces; }

	/*
	* close connection to the external stockfish engine
	*/
	void closeEngine();

	/*
	 * return all the valid moves for a piece
	 *
	 * @param piece : The current piece we are interested in retrieving valid moves for
	 *
	 * @return pieceMoves : vector of valid moves
	 */
	vector<Position> getValidMovesForPiece(shared_ptr<Piece> piece);

	/*
	* Sets the position of the provided Piece and determines if a capture move was performed or is piece eligible to make an enPassant move
	*
	* @param selectedPiece : The currently selectedPiece that we are setting the position for
	* @param newPosition : The new position for the selectedPiece
	*/
	void setPositionOfPiece(shared_ptr<Piece> selectedPiece , Position newPosition);

	/*
	* If there exists an opponent piece occupying the new position for the selected piece
	* capture the piece and erase it from the opponents set
	*
	* @param: position : the new position for the selectedPiece and the position to be checked if it occupies an opponents piece
	* @param selectedPiece: the piece making the move
	*/
	void capturePieceIfApplicable(Position position, shared_ptr<Piece> selectedPiece);

	/*
	* perform a castle move
	*
	* @param kingPiece : king to undergo castling
	* @param newPosition: the position the king is moving to
	*
	* @return true if castling was successful
	*/
	bool castleMove(shared_ptr<Piece> kingPiece, Position newPosition);

	/*
	* Determine if pawn has made a two tile move , if so , raise flag so an en-Passant move can be made
	*
	* @param pawnPiece
	* @param newPosition
	*
	* @return shared_ptr<Piece>
	*/
	shared_ptr<Piece> checkIfPawnHasMovedTwoTiles(shared_ptr<Piece> pawnPiece , Position newPosition);

	/*
	* determine if king is in check or checkmate and return results
	*
	* @param currentTeamTurn
	*
	* return tuple : Position (position of king in check)
	*			     bool (is king in check)
	*				 bool (is king in checkmate)
	*/
	tuple<Position *, bool, bool> checkForCheckOrCheckmate(string currentTeamTurn);

	/*
	* return the opponent side
	*
	* @param pieceSide : the current side of the piece ( black or white)
	*
	* @return string : return opponent side
	*/
	string getOpponentSide(string pieceSide);

	/*
	* sync board with set pieces and their locations
	*/
	void syncBoard();

	/*
	* translate a move into chess notation as the coordinate system implemented differ to chess notation
	* this allows communication with the chess engine
	*
	* @param piece: the current piece performing the move
	* @param newPosition: the new position for the piece
	*
	* @return translatedMove: the move in chess notation
	*/
	string translateMoveToChessNotation(shared_ptr<Piece> piece , Position newPosition);

	/*
	* Acquire next engine move for the black set piececs
	*
	* @param whiteMove: the last move white made formatted in chess notation
	*
	* @return tuple : @Position: the position of the piece to be moved
					  @Position: the new position of the piece to be moved
					  @string: the move performated formatted in chess notation
						** returned in the specified order
	*/
	tuple<Position, Position, string> getEngineMoveForBlack(string whiteMove);

	/*
	* retrieve possible moves for the provided piece
	*
	* @param currentPiece : the piece we are attempting to retrieve the possible moves for
	* @param opponentSide : the opponent of the currentPiece
	*
	* return vector<Position> : return a collection with the possible moves for the piece
	*/
	vector<Position> getPossibleMovesForPiece(shared_ptr<Piece> currentPiece, string opponentSide);

	/* check if there a pawn has reached the promotion rank , if so promote to quee
	*
	@ param currentSideToPlay
	*/
	void checkForPawnPromotion(string currentSideToPlay);

	/*
	* retrieve all possible attacks move for pawn , including en Passant capturing if applicable
	*
	* @param pieceInFocus : current piece in focus , guaranteed to be a pawn as its name field has to include a pawn before calling this func
	* @param pieceSide : the side of the current piece
	* @param isKingPiece : when calculating allowed movement for a king piece , this bool flag will be used to determine if there are any tiles attacked
	*					   by an opponent pawn , hence king cannot move to that tile.
	* @param checkForEnPassantMove : check for an enpassant move or not
	*
	* @return vector<Position> : collection of attacked positions by pawns.
*/
	vector<Position> getAttackMovesForPawn(shared_ptr<Piece> pieceInFocus, string pieceSide, bool isKingPiece , bool checkForEnPassantMove);
	
	/*
	* return possible moves for a piece
	*
	* @param piece
	*
	* @return possibleMovePositions
	*/
	vector<Position> computePossibleMoves(shared_ptr<Piece> piece);

	/*
	* retrieve possible moves for the provided piece
	*
	* @param currentPiece : the piece we are attempting to retrieve the possible moves for
	* @param opponentSide : the opponent of the currentPiece
	*
	* return vector<Position> : return a collection with the possible moves for the piece
	*/
	vector<Position> getPossibleMovesForKingPiece(shared_ptr<Piece> piece, string opponentSide);

	/*
	* King is unable to capture pieces that are guarded by other opponent pieces , this function ensures that
	*
	* @param currentPiece : calculate if this piece is guarded or not
	*
	* @return guardedPositions : collection of guarded positions
	*/
	vector<Position> getGuardedPositionsForPiece(shared_ptr<Piece> currentPiece);
	
	/*
	* returns the king piece position
	*
	* @param pieceSide : white or black king
	*
	* @return position of king
	*/
	Position* getKingPosition(string pieceSide);

	/*
	* certain moves are not allowed as they put their king underattack , this function ensures that this is never the case
	*
	* @param piece
	* @param possibleMoves : collection of moves that are possible to be made by the provided piece
	*/	vector<Position> removeUnallowableMoves(shared_ptr<Piece> piece, vector<Position> possiblePieceMoves);

	/*
	* Check if king is allowed to castle
	*
	* @param king
	* @param attackedPositions : attackedPositions by opponent side
	*
	* @return castlingMoves : collection of possible castling moves
	*/	vector<Position> getKingCastlingMoves(King king, vector<Position> attackPositions);

};
