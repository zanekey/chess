
#include<SFML/Graphics.hpp>
#include "CreateChessBoardAndSets.h"
#include "Piece.h"
#include "Pawn.cpp"
#include "Rook.cpp"
#include "Knight.cpp"
#include "Bishop.cpp"
#include "Queen.cpp"
#include "King.cpp"

using namespace sf;

/*
* draw a the background of a chess board
*/
VertexArray createChessBoardBackground() {
	VertexArray vertexArray;
	// size and widths of tiles
	const int TILE_SIZE = 100;
	const int TILE_TYPES = 2;
	const int NO_OF_VERTICES = 4;


	int boardWidth = 800 / TILE_SIZE;
	int boardHeight = 800 / TILE_SIZE;

	vertexArray.setPrimitiveType(Quads);

	// set the size of the vertex array
	vertexArray.resize(boardWidth * boardHeight * NO_OF_VERTICES);
	sf::Color customBlueColor(33, 105, 156);

	int currentVertex = 0;
	int oddOrEven = 0;
	for (int w = 0; w < boardWidth ; w++)
	{
		for (int h = 0; h < boardHeight; h++)
		{
			// Position each vertex in the current quad
			vertexArray[currentVertex + 0].position = Vector2f(w * TILE_SIZE, h * TILE_SIZE);
			vertexArray[currentVertex + 1].position = Vector2f((w * TILE_SIZE) + TILE_SIZE, h * TILE_SIZE);
			vertexArray[currentVertex + 2].position = Vector2f((w * TILE_SIZE) + TILE_SIZE, (h * TILE_SIZE) + TILE_SIZE);
			vertexArray[currentVertex + 3].position = Vector2f((w * TILE_SIZE), (h * TILE_SIZE) + TILE_SIZE);

			int verticalOffset = TILE_SIZE / 2;
			if (oddOrEven % 2 == 0) {
				vertexArray[currentVertex + 0].color = sf::Color::White;
				vertexArray[currentVertex + 1].color = sf::Color::White;
				vertexArray[currentVertex + 2].color = sf::Color::White;
				vertexArray[currentVertex + 3].color = sf::Color::White;
				
			}
			else
			{
				vertexArray[currentVertex + 0].color = customBlueColor;
				vertexArray[currentVertex + 1].color = customBlueColor;
				vertexArray[currentVertex + 2].color = customBlueColor;
				vertexArray[currentVertex + 3].color = customBlueColor;
			}

			// Position ready for the next for vertices
			currentVertex += NO_OF_VERTICES;
			oddOrEven++;
		}

		oddOrEven++;
	}


	return vertexArray;

}

/*
* highlight the king tile if it is under attack
*
* @param position : position of the king to be highlighted
* 
* @return vertexArray 
*/
VertexArray drawCheck(Position position) {
	VertexArray checkVA;

	sf::Color darkRed(179, 2, 2);
	checkVA.setPrimitiveType(Quads);
	checkVA.resize(4 * TILE_SIZE);

	int x = position.getXSpritePosition();
	int y = position.getYSpritePosition();

	checkVA[0].position = Vector2f(x, y);
	checkVA[1].position = Vector2f(x + TILE_SIZE, y);
	checkVA[2].position = Vector2f(x + TILE_SIZE, y + TILE_SIZE);
	checkVA[3].position = Vector2f(x, y + TILE_SIZE);

	checkVA[0].color = darkRed;
	checkVA[1].color = darkRed;
	checkVA[2].color = darkRed;
	checkVA[3].color = darkRed;

	return checkVA;
}

/*
* when a piece moves , highlight the previous position with one color and the new position with a new colour for a better user experience 
* 
* @param oldPosition
* @param newPosition
*
* @return vertexArray
*/
VertexArray drawMoveTracer(Position oldPosition, Position newPosition) {

	sf::Color goldColour(252, 165, 3);
	sf::Color lightGoldColor(255, 218, 97);
	VertexArray moveTracer;

	moveTracer.setPrimitiveType(Quads);
	moveTracer.resize(4 * TILE_SIZE);

	int x = oldPosition.getXSpritePosition();
	int y = oldPosition.getYSpritePosition();

	moveTracer[0].position = Vector2f(x, y);
	moveTracer[1].position = Vector2f(x + TILE_SIZE, y);
	moveTracer[2].position = Vector2f(x + TILE_SIZE, y + TILE_SIZE);
	moveTracer[3].position = Vector2f(x, y + TILE_SIZE);

	moveTracer[0].color = lightGoldColor;
	moveTracer[1].color = lightGoldColor;
	moveTracer[2].color = lightGoldColor;
	moveTracer[3].color = lightGoldColor;


	// new posiiton

	x = newPosition.getXSpritePosition();
	y = newPosition.getYSpritePosition();

	moveTracer[4].position = Vector2f(x, y);
	moveTracer[5].position = Vector2f(x + TILE_SIZE, y);
	moveTracer[6].position = Vector2f(x + TILE_SIZE, y + TILE_SIZE);
	moveTracer[7].position = Vector2f(x, y + TILE_SIZE);

	moveTracer[4].color = goldColour;
	moveTracer[5].color = goldColour;
	moveTracer[6].color = goldColour;
	moveTracer[7].color = goldColour;

	return moveTracer;
}

/* draws the possibles moves of a piece on the GUI
*
* @param selectedPiecePossibleMoves : collection of the moves possible for a piece
*
* @return possibleMoves : vertexArray with the graphical representation of the moves possible
*/
VertexArray drawPossibleMovesOnBoard(std::vector<Position> selectedPiecePossibleMoves) {
	VertexArray possibleMoves;

	possibleMoves.setPrimitiveType(Quads);
	possibleMoves.resize(4 * 1000);

	int currentVertex = 0;
	for (int i = 0; i < selectedPiecePossibleMoves.size(); i++) {

		auto cs = selectedPiecePossibleMoves.at(i);
		int x = cs.getXSpritePosition();
		int y = cs.getYSpritePosition();

		int TILE_SIZE = 100;

		possibleMoves[currentVertex + 0].position = Vector2f(x , y );
		possibleMoves[currentVertex + 1].position = Vector2f(x  + TILE_SIZE, y );
		possibleMoves[currentVertex + 2].position = Vector2f(x  + TILE_SIZE, y  + TILE_SIZE);
		possibleMoves[currentVertex + 3].position = Vector2f(x , y  + TILE_SIZE);

		possibleMoves[currentVertex + 0].color = sf::Color::Green;
		possibleMoves[currentVertex + 1].color = sf::Color::Green;
		possibleMoves[currentVertex + 2].color = sf::Color::Green;
		possibleMoves[currentVertex + 3].color = sf::Color::Green;
		currentVertex += 4;
	}

	return possibleMoves;
}

/* Init black set pieces to their appropiate positions
*
* return whiteSetPieces: vector collection of a complete white set
*/
vector<shared_ptr<Piece>>  getWhiteSetPieces() {
	vector<shared_ptr<Piece>> whiteSetPieces;

	// declare pieces respective positions
	Position pawnPos1(0, 6);
	Position pawnPos2(1, 6);
	Position pawnPos3(2, 6);
	Position pawnPos4(3, 6);
	Position pawnPos5(4, 6);
	Position pawnPos6(5, 6);
	Position pawnPos7(6, 6);
	Position pawnPos8(7, 6);

	Position kingPos(4, 7);
	Position queenPos(3, 7);
	Position rookPos1(0, 7);
	Position rookPos2(7, 7);
	Position knightPos1(6, 7);
	Position knightPos2(1, 7);
	Position bishopPos1(2, 7);
	Position bishopPos2(5, 7);
	
	//create pieces with their respective positions
	shared_ptr<Piece> pawn1(new Pawn(pawnPos1, WHITE_SIDE));
	shared_ptr<Piece> pawn2(new Pawn(pawnPos2, WHITE_SIDE));
	shared_ptr<Piece> pawn3(new Pawn(pawnPos3, WHITE_SIDE));
	shared_ptr<Piece> pawn4(new Pawn(pawnPos4, WHITE_SIDE));
	shared_ptr<Piece> pawn5(new Pawn(pawnPos5, WHITE_SIDE));
	shared_ptr<Piece> pawn6(new Pawn(pawnPos6, WHITE_SIDE));
	shared_ptr<Piece> pawn7(new Pawn(pawnPos7, WHITE_SIDE));
	shared_ptr<Piece> pawn8(new Pawn(pawnPos8, WHITE_SIDE));

	shared_ptr<Piece> rook1(new Rook(rookPos1, WHITE_SIDE));
	shared_ptr<Piece> rook2(new Rook(rookPos2, WHITE_SIDE));
	shared_ptr<Piece> knight1(new Knight(knightPos1, WHITE_SIDE));
	shared_ptr<Piece> knight2(new Knight(knightPos2, WHITE_SIDE));
	shared_ptr<Piece> bishop1(new Bishop(bishopPos1, WHITE_SIDE));
	shared_ptr<Piece> bishop2(new Bishop(bishopPos2, WHITE_SIDE));
	shared_ptr<Piece> queen(new Queen(queenPos, WHITE_SIDE));
	shared_ptr<Piece> king(new King(kingPos, WHITE_SIDE));

	// add set pieces to the set
	whiteSetPieces.insert(whiteSetPieces.begin(), pawn1);
	whiteSetPieces.insert(whiteSetPieces.begin(), pawn2);
	whiteSetPieces.insert(whiteSetPieces.begin(), pawn3);
	whiteSetPieces.insert(whiteSetPieces.begin(), pawn4);
	whiteSetPieces.insert(whiteSetPieces.begin(), pawn5);
	whiteSetPieces.insert(whiteSetPieces.begin(), pawn6);
	whiteSetPieces.insert(whiteSetPieces.begin(), pawn7);
	whiteSetPieces.insert(whiteSetPieces.begin(), pawn8);

	whiteSetPieces.insert(whiteSetPieces.begin(), rook1);
	whiteSetPieces.insert(whiteSetPieces.begin(), rook2);
	whiteSetPieces.insert(whiteSetPieces.begin(), knight1);
	whiteSetPieces.insert(whiteSetPieces.begin(), knight2);
	whiteSetPieces.insert(whiteSetPieces.begin(), bishop1);
	whiteSetPieces.insert(whiteSetPieces.begin(), bishop2);
	whiteSetPieces.insert(whiteSetPieces.begin(), queen);
	whiteSetPieces.insert(whiteSetPieces.begin(), king);

	return whiteSetPieces;
}

/* Init black set pieces to their appropiate positions
*
* return blackSetPieces: vector collection of a complete black set
*/
vector<shared_ptr<Piece>>  getBlackSetPieces() {
	vector<shared_ptr<Piece>> blackSetPieces;

	// init respective positions
	Position pawnPos1(0, 1);
	Position pawnPos2(1, 1);
	Position pawnPos3(2, 1);
	Position pawnPos4(3, 1);
	Position pawnPos5(4, 1);
	Position pawnPos6(5, 1);
	Position pawnPos7(6, 1);
	Position pawnPos8(7, 1);

	Position kingPos(4, 0);
	Position queenPos(3, 0);
	Position rookPos1(0, 0);
	Position rookPos2(7, 0);
	Position knightPos1(6, 0);
	Position knightPos2(1, 0);
	Position bishopPos1(2, 0);
	Position bishopPos2(5, 0);


	// create pieces with their respective positions
	shared_ptr<Piece> pawn1(new Pawn(pawnPos1, BLACK_SIDE));
	shared_ptr<Piece> pawn2(new Pawn(pawnPos2, BLACK_SIDE));
	shared_ptr<Piece> pawn3(new Pawn(pawnPos3, BLACK_SIDE));
	shared_ptr<Piece> pawn4(new Pawn(pawnPos4, BLACK_SIDE));
	shared_ptr<Piece> pawn5(new Pawn(pawnPos5, BLACK_SIDE));
	shared_ptr<Piece> pawn6(new Pawn(pawnPos6, BLACK_SIDE));
	shared_ptr<Piece> pawn7(new Pawn(pawnPos7, BLACK_SIDE));
	shared_ptr<Piece> pawn8(new Pawn(pawnPos8, BLACK_SIDE));

	shared_ptr<Piece> rook1(new Rook(rookPos1, BLACK_SIDE));
	shared_ptr<Piece> rook2(new Rook(rookPos2, BLACK_SIDE));
	shared_ptr<Piece> knight1(new Knight(knightPos1, BLACK_SIDE));
	shared_ptr<Piece> knight2(new Knight(knightPos2, BLACK_SIDE));
	shared_ptr<Piece> bishop1(new Bishop(bishopPos1, BLACK_SIDE));
	shared_ptr<Piece> bishop2(new Bishop(bishopPos2, BLACK_SIDE));
	shared_ptr<Piece> queen(new Queen(queenPos, BLACK_SIDE));
	shared_ptr<Piece> king(new King(kingPos, BLACK_SIDE));
	
	// add pieces to the set
	blackSetPieces.insert(blackSetPieces.begin(), pawn1);
	blackSetPieces.insert(blackSetPieces.begin(), pawn2);
	blackSetPieces.insert(blackSetPieces.begin(), pawn3);
	blackSetPieces.insert(blackSetPieces.begin(), pawn4);
	blackSetPieces.insert(blackSetPieces.begin(), pawn5);
	blackSetPieces.insert(blackSetPieces.begin(), pawn6);
	blackSetPieces.insert(blackSetPieces.begin(), pawn7);
	blackSetPieces.insert(blackSetPieces.begin(), pawn8);

	blackSetPieces.insert(blackSetPieces.begin(), rook1);
	blackSetPieces.insert(blackSetPieces.begin(), rook2);
	blackSetPieces.insert(blackSetPieces.begin(), knight1);
	blackSetPieces.insert(blackSetPieces.begin(), knight2);
	blackSetPieces.insert(blackSetPieces.begin(), bishop1);
	blackSetPieces.insert(blackSetPieces.begin(), bishop2);
	blackSetPieces.insert(blackSetPieces.begin(), queen);
	blackSetPieces.insert(blackSetPieces.begin(), king);

	return blackSetPieces;
}

/*
* rectangle to be displayed when match has ended
*
* @return resultRectangle
*/
RectangleShape getResultRectangle() {
	RectangleShape resultRectangle;

	resultRectangle.setPosition(sf::Vector2f(250, 200));
	resultRectangle.setSize(sf::Vector2f(200, 100));
	resultRectangle.setFillColor(sf::Color::Black);
	resultRectangle.setOutlineColor(sf::Color::Magenta);
	resultRectangle.setOutlineThickness(2.0);

	return resultRectangle;
}

/*
* Result to be drawn over resultRectangle when match has concluded
*
* @param victoriousSide
* @param font
*
* @return resultText
*/
sf::Text getResultText(string victoriousSide , Font & font) {

	sf::Text resultText;
	resultText.setPosition(sf::Vector2f(275, 220));
	resultText.setCharacterSize(20);
	
	resultText.setFont(font);
	resultText.setFillColor(sf::Color::White);

	resultText.setString(victoriousSide + " wins");

	return resultText;

}