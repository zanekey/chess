#include "Piece.h"

class King : public Piece {
private:
	bool castlingRight;
	vector<int> kingSideCastleTilesX = { 1 , 2 };
	vector<int> kingSideCastleTilesY = { 0 , 0 };
	vector<int> queenSideCastleTilesX = { -1 , -2 , -3 };
	vector<int> queenSideCastleTilesY = { 0 , 0  , 0};
	Position kingSideCastlePosition;
	Position queenSideCastlePosition;

public:
	King(Position initPosition, string side) : Piece(initPosition, side) {

		castlingRight = true;
		imgFile = KING_PIECE_IMG_FILE;
		Piece::setSpriteSettings();

		pieceName = side + KING;
		limitedMovement = true;
		xMoveOffsets = { 1, 1, -1, -1 , 0 , 1 , 0 , -1 };
		yMoveOffsets = { 1, -1 , -1 , 1 , 1 , 0 , -1 , 0 };

		kingSideCastlePosition = Position(initPosition.getXPosition() + 2, initPosition.getYPosition());
		queenSideCastlePosition = Position(initPosition.getXPosition() - 2, initPosition.getYPosition());
		
	}

	bool getCastlingRight() { return castlingRight; }

	vector<int> getKingSideCastleTilesX() { return kingSideCastleTilesX; }
	vector<int> getKingSideCastleTilesY() { return kingSideCastleTilesY; }
	vector<int> getQueenSideCastleTilesX() { return queenSideCastleTilesX; }
	vector<int> getQueenSideCastleTilesY() { return queenSideCastleTilesY; }
	Position getKingSideCastlePosition() { return kingSideCastlePosition; }
	Position getQueenSideCastlePosition() { return queenSideCastlePosition; }
};