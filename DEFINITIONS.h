#pragma once
#include <string>
using namespace std;


const int TILE_SIZE = 100;
const int BOARD_DIMENSION = 8;
const int BOARD_SIZE_PIXEL = TILE_SIZE * BOARD_DIMENSION;
const float SPRITE_SCALE = .3;
const string WHITE_KNIGHT = "graphics/pieces/WhiteKnight.png";
const string PIECE_DIR = "graphics/pieces/";
const string WHITE_SIDE = "white";
const string BLACK_SIDE = "black";
const string KING_PIECE_IMG_FILE = "/king.png";
const string QUEEN_PIECE_IMG_FILE = "/queen.png";
const string KNIGHT_PIECE_IMG_FILE = "/knight.png";
const string BISHOP_PIECE_IMG_FILE = "/bishop.png";
const string ROOK_PIECE_IMG_FILE = "/rook.png";
const string PAWN_PIECE_IMG_FILE = "/pawn.png";
const string KING = "_king";
const string QUEEN = "_queen";
const string BISHOP = "_bishop";
const string KNIGHT = "_knight";
const string ROOK = "_rook";
const string PAWN = "_pawn";
const string EMPTY = "empty";
const char X_CHESS_NOTATION[BOARD_DIMENSION] = { 'a' , 'b' , 'c' , 'd' , 'e' , 'f' , 'g' , 'h' };
const char Y_CHESS_NOTATION[BOARD_DIMENSION] = { '8' , '7' , '6' , '5' , '4' , '3' , '2' , '1' };
