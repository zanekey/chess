#pragma once
#include <list>
#include <thread>
#include <chrono>
#include "CreateChessBoardAndSets.h"
#include <iostream>
#include "Connector.hpp"
#include "GameManager.h"
#include "Pawn.cpp"
#include "Rook.cpp"
#include "Queen.cpp"


/*
* @author zane keyan
*/


/*
* Creates instance of GameManager
*/
GameManager::GameManager() {
	// connect to opponent engine that will in control of black set
	char ENGINE_FILENAME[] = "stockfish.exe";
	ConnectToEngine(ENGINE_FILENAME);

	// initiate sets and sync board
	whiteSetPieces = getWhiteSetPieces();
	blackSetPieces = getBlackSetPieces();
	syncBoard();
};

/*
 * return all the valid moves for a piece
 *
 * @param piece : The current piece we are interested in retrieving valid moves for
 *
 * @return pieceMoves : vector of valid moves
 */
vector<Position> GameManager::getValidMovesForPiece(shared_ptr<Piece> piece) {
	vector<Position> pieceMoves = computePossibleMoves(piece);
	syncBoard();
	return pieceMoves;
}

/*
* Sets the position of the provided Piece and determines if a capture move was performed or is piece eligible to make an enPassant move
*
* @param selectedPiece : The currently selectedPiece that we are setting the position for
* @param newPosition : The new position for the selectedPiece
*/
void GameManager::setPositionOfPiece(shared_ptr<Piece> selectedPiece , Position newPosition) {

	// determine if a castle move was perfomed
	if (selectedPiece->getPieceName().find(KING) != std::string::npos) 
		if (castleMove(selectedPiece, newPosition)) { syncBoard(); return; }

	//determine if a capture move has been performed or flag in an en Passant oppurtunity exists
	capturePieceIfApplicable(newPosition, selectedPiece);
	enPassantPawn = checkIfPawnHasMovedTwoTiles(selectedPiece, newPosition);

	// set the position for the piece 
	selectedPiece->setPosition(newPosition);
	//selectedPiece->setSpritePosition(newPosition.getXSpritePosition(), newPosition.getYSpritePosition());
	syncBoard();
}

/* check if there a pawn has reached the promotion rank , if so promote to quee
* 
@ param currentSideToPlay
*/
void GameManager::checkForPawnPromotion(string currentSideToPlay) {

	// deteremine what set to focus on 
	vector<shared_ptr<Piece>> & setPieces = currentSideToPlay == WHITE_SIDE ? whiteSetPieces : blackSetPieces;

	// promotion rank relative to the current side
	int promotionRank = currentSideToPlay == WHITE_SIDE ? 0 : 7;

	// index , to remove pawn from list
	int i = 0;
	for (shared_ptr<Piece> currentPiece : setPieces) {

		if (currentPiece->getPieceName().find(PAWN) != std::string::npos && currentPiece->getPosition().getYPosition() == promotionRank) {
			Position newPos(currentPiece->getPosition().getXPosition(), currentPiece->getPosition().getYPosition());
			shared_ptr<Piece> queen(new Queen(newPos , currentSideToPlay));
			setPieces.erase(setPieces.begin() + i);
			setPieces.insert(setPieces.begin(), queen);
			return;
		}
		i++;
	}

	return;
}

/*
* determine if king is in check or checkmate and return results
*
* @param currentTeamTurn
*
* return tuple : Position (position of king in check) 
*			     bool (is king in check)
*				 bool (is king in checkmate)
*/
tuple<Position*, bool, bool> GameManager::checkForCheckOrCheckmate(string currentTeamTurn) {

	vector<shared_ptr<Piece>> opponentSet = whiteSetPieces;
	string opponetSide = WHITE_SIDE;
	vector<shared_ptr<Piece>> allySet = blackSetPieces;
	bool kingUnderAttack = false;
	bool checkMate = false;

	if (currentTeamTurn == WHITE_SIDE) {
		opponentSet = blackSetPieces;
		opponetSide = BLACK_SIDE;
		allySet = whiteSetPieces;
	}

	vector<Position> allAttackMoves;
	Position * kingPosition = getKingPosition(currentTeamTurn);

	// check if king is in check
	for (shared_ptr<Piece> currentPiece : opponentSet) {

		auto attackMoves = getValidMovesForPiece(currentPiece);
		for (Position attackMove : attackMoves) {

			if (attackMove.getXPosition() == kingPosition->getXPosition() && attackMove.getYPosition() == kingPosition->getYPosition())
					kingUnderAttack = true;
		}

		allAttackMoves.insert(allAttackMoves.begin(), attackMoves.begin(), attackMoves.end());
	}

	// check if king is in checkmate
	vector<Position> allyMoves;
	for (shared_ptr<Piece> currentPiece : allySet) {
		auto attackMoves = getValidMovesForPiece(currentPiece);
		allyMoves.insert(allyMoves.begin(), attackMoves.begin(), attackMoves.end());
	}

	if (allyMoves.size() == 0) checkMate = true;

	return make_tuple(kingPosition, kingUnderAttack, checkMate);

}

/* Close the connection to the external opponent Engine */
void GameManager::closeEngine() { CloseConnection(); }

/*
* Acquire next engine move for the black set piececs 
*
* @param whiteMove: the last move white made formatted in chess notation
*
* @return tuple : @Position: the position of the piece to be moved
				  @Position: the new position of the piece to be moved
				  @string: the move performated formatted in chess notation
					** returned in the specified order
*/
tuple<Position , Position , string> GameManager::getEngineMoveForBlack(string whiteMove) {
	// retrieve next move from engine
	string blacksNextMove = getNextMove(whiteMove);

	// piece to be moved posiiton
	char pieceXPos = blacksNextMove[0];
	char pieceYPos = blacksNextMove[1];

	// new position for piece to be moved
	char newXPos = blacksNextMove[2];
	char newYPos = blacksNextMove[3];
	
	// translate move to chess notation
	int x = std::distance(X_CHESS_NOTATION, std::find(std::begin(X_CHESS_NOTATION), std::end(X_CHESS_NOTATION), pieceXPos));
	int y = std::distance(Y_CHESS_NOTATION, std::find(std::begin(Y_CHESS_NOTATION), std::end(Y_CHESS_NOTATION), pieceYPos));
	int newX = std::distance(X_CHESS_NOTATION, std::find(std::begin(X_CHESS_NOTATION), std::end(X_CHESS_NOTATION), newXPos));
	int newY = std::distance(Y_CHESS_NOTATION, std::find(std::begin(Y_CHESS_NOTATION), std::end(Y_CHESS_NOTATION), newYPos));
	
	// scan and find the position with the provided piece positions
	shared_ptr<Piece> currentPiece = nullptr;
	for (shared_ptr<Piece> piece : blackSetPieces){
		// if a piece is found
		if (piece->getPosition().getXPosition() == x && piece->getPosition().getYPosition() == y) {
			currentPiece = piece;
			break;
		}
	}

	//init tuple to be returned
	Position newPosition(newX, newY);
	tuple<Position, Position, string> tuple = make_tuple(currentPiece->getPosition(), newPosition, blacksNextMove);
	
	// move the specified piece to the specified position
	setPositionOfPiece(currentPiece, newPosition);
	syncBoard();

	return tuple;
}

/*
* translate a move into chess notation as the coordinate system implemented differ to chess notation
* this allows communication with the chess engine
*
* @param piece: the current piece performing the move
* @param newPosition: the new position for the piece 
*
* @return translatedMove: the move in chess notation
*/
string GameManager::translateMoveToChessNotation( shared_ptr<Piece> piece , Position newPosition) {

	Position oldPosition = piece->getPosition();

	char oldXTranslated = X_CHESS_NOTATION[oldPosition.getXPosition()];
	char oldYTranslated = Y_CHESS_NOTATION[oldPosition.getYPosition()];
	char translatedXvalue = X_CHESS_NOTATION[newPosition.getXPosition()];
	char translatedYvalue = Y_CHESS_NOTATION[newPosition.getYPosition()];

	string translatedMove;
	translatedMove.push_back(oldXTranslated);
	translatedMove.push_back(oldYTranslated);
	translatedMove.push_back(translatedXvalue);
	translatedMove.push_back(translatedYvalue);

	return translatedMove;
}

/* 
* Determine if pawn has made a two tile move , if so , raise flag so an en-Passant move can be made
*
* @param pawnPiece
* @param newPosition
*
* @return shared_ptr<Piece>
*/
shared_ptr<Piece> GameManager::checkIfPawnHasMovedTwoTiles(shared_ptr<Piece> pawnPiece , Position newPosition) {
	// if piece is not a pawn piece
	if (pawnPiece->getPieceName().find(PAWN) == std::string::npos) return false;

	// determine if pawn has move by two tiles
	if (abs(newPosition.getYPosition() - pawnPiece->getPosition().getYPosition()) == 2) return pawnPiece;
		
	//otherwise reset properties
	return nullptr;
}

/*
* If there exists an opponent piece occupying the new position for the selected piece
* capture the piece and erase it from the opponents set
*
* @param: position : the new position for the selectedPiece and the position to be checked if it occupies an opponents piece
* @param selectedPiece: the piece making the move
*/
void GameManager::capturePieceIfApplicable(Position position, shared_ptr<Piece> selectedPiece) {
	// determine which side is the opponent side
	string opponentSide = getOpponentSide(selectedPiece->getPieceSide());

	//determine which side is the opponent side
	vector<shared_ptr<Piece>>* currentSet = &whiteSetPieces;
	if (opponentSide == BLACK_SIDE) currentSet = &blackSetPieces;

	// if theres a pawn attempting to make an en Passant capture , set the position to the position of the en passant piece so that it will be erased by the next loop
	if ( enPassantPawn != nullptr &&  selectedPiece->getPieceName().find(PAWN) != std::string::npos && position.getXPosition() == enPassantPawn->getPosition().getXPosition()
		&& position.getYPosition() == enPassantPawn->getPosition().getYPosition() - enPassantPawn->getYMoveOffsets()[0]) {
			position = enPassantPawn->getPosition();
	}

	// determine if a piece already occupies the new location of the selected piece , if so , capture the piece and remove it from its respective set
	for (int i = 0; i < currentSet->size(); i++)
		if (currentSet->at(i)->getPosition().getXPosition() == position.getXPosition() && currentSet->at(i)->getPosition().getYPosition() == position.getYPosition())
				currentSet->erase(currentSet->begin() + i);
}

/*
* perform a castle move 
*
* @param kingPiece : king to undergo castling
* @param newPosition: the position the king is moving to
*
* @return true if castling was successful
*/
bool GameManager::castleMove(shared_ptr<Piece> kingPiece, Position newPosition) {

	int positionDifferenceX = newPosition.getXPosition() - kingPiece->getPosition().getXPosition();
	vector<shared_ptr<Piece>> allySetPieces = whiteSetPieces;

	if (kingPiece->getPieceSide() == BLACK_SIDE) allySetPieces = blackSetPieces;

	if ( abs(positionDifferenceX) != 2) return false;

	bool kingSideCastleMove = true;

	if (positionDifferenceX == -2) kingSideCastleMove = false;
	
	// if difference is positive 2 , this indicates a king side castle move has been performed
	kingPiece -> setPosition(newPosition);
	kingPiece-> setSpritePosition(newPosition.getXSpritePosition(), newPosition.getYSpritePosition());

	for (shared_ptr<Piece> piece : allySetPieces) {
		if ( kingSideCastleMove && piece->getPieceName().find(ROOK) != std::string::npos && piece->getPosition().getXPosition() == 7) {

			Position newRookPosition(5, piece->getPosition().getYPosition());
			piece->setPosition(newRookPosition);
			piece->setSpritePosition(newRookPosition.getXSpritePosition(), newRookPosition.getYSpritePosition());
			return true;
		}

		if ( ! kingSideCastleMove && piece->getPieceName().find(ROOK) != std::string::npos && piece->getPosition().getXPosition() == 0) {
			Position newRookPosition(3, piece->getPosition().getYPosition());
			piece->setPosition(newRookPosition);
			piece->setSpritePosition(newRookPosition.getXSpritePosition(), newRookPosition.getYSpritePosition());
			return true;
		}
	}
	return false;
}

/*
* return the opponent side
* 
* @param pieceSide : the current side of the piece ( black or white)
*
* @return string : return opponent side
*/
string GameManager::getOpponentSide(string pieceSide) {
	if (pieceSide == WHITE_SIDE) return BLACK_SIDE;
	return WHITE_SIDE;
}

/*
* sync board with set pieces and their locations
*/
void GameManager::syncBoard() {
	//populate the whole board with empty values initially
	for (int x = 0; x < BOARD_DIMENSION; x++) for (int y = 0; y < BOARD_DIMENSION; y++) boardReference[x][y] = EMPTY;

	//sync white pieces
	for (auto s : whiteSetPieces) boardReference[s->getPosition().getXPosition()][s->getPosition().getYPosition()] = s->getPieceName();

	//sync black pieces
	for (auto s : blackSetPieces) boardReference[s->getPosition().getXPosition()][s->getPosition().getYPosition()] = s->getPieceName();
}

/*
* retrieve possible moves for the provided piece
*
* @param currentPiece : the piece we are attempting to retrieve the possible moves for
* @param opponentSide : the opponent of the currentPiece
*
* return vector<Position> : return a collection with the possible moves for the piece
*/
vector<Position> GameManager::getPossibleMovesForPiece(shared_ptr<Piece> currentPiece, string opponentSide) {
	// init and set fields
	vector<Position> possibleMovePositions;

	if (currentPiece -> getPieceName().find(PAWN) != std::string::npos)
	possibleMovePositions = getAttackMovesForPawn(currentPiece, currentPiece -> getPieceSide(), false , true);

	// iterate over piece offsets
	for (int i = 0; i < currentPiece-> getXMoveOffsets().size(); i++) {

		// the x value will amplify the offsets accordingly to acquire the unlimited movement 
		int x = 0;
		int newX = currentPiece -> getPosition().getXPosition() + currentPiece-> getXMoveOffsets()[i] + (currentPiece-> getXMoveOffsets()[i] * x);
		int newY = currentPiece ->getPosition().getYPosition() + currentPiece-> getYMoveOffsets()[i] + (currentPiece-> getYMoveOffsets()[i] * x);
		
		// ensure position values are within board boundaries
		while (newX < BOARD_DIMENSION && newY < BOARD_DIMENSION && newX >= 0 && newY >= 0) {

			// if piece movement collides with another ally piece
			if (boardReference[newX][newY].substr(0, 5).compare(currentPiece -> getPieceSide()) == 0) { x++; break; }

			//if piece is a pawn and its movement is blocked
			if (currentPiece-> getPieceName().find(PAWN) != std::string::npos && boardReference[newX][newY] != EMPTY)
				return possibleMovePositions;

			//declare new position
			Position newPos(newX, newY);
			possibleMovePositions.insert(possibleMovePositions.begin(), newPos);

			//break out of loop if the piece movement is limited or movement is intersected
			if (boardReference[newX][newY].substr(0, 5).compare(opponentSide) == 0 || currentPiece -> isMovementLimited()) break;

			newX = currentPiece -> getPosition().getXPosition() + currentPiece-> getXMoveOffsets()[i] + (currentPiece-> getXMoveOffsets()[i] * x);
			newY = currentPiece -> getPosition().getYPosition() + currentPiece -> getYMoveOffsets()[i] + (currentPiece-> getYMoveOffsets()[i] * x);
			x++;
		}
	}
	return possibleMovePositions;
}

/*
* retrieve all possible attacks move for pawn , including en Passant capturing if applicable
*
* @param pieceInFocus : current piece in focus , guaranteed to be a pawn as its name field has to include a pawn before calling this func
* @param pieceSide : the side of the current piece
* @param isKingPiece : when calculating allowed movement for a king piece , this bool flag will be used to determine if there are any tiles attacked
*					   by an opponent pawn , hence king cannot move to that tile.
* @param checkForEnPassantMove : check for an enpassant move or not
*
* @return vector<Position> : collection of attacked positions by pawns.
*/
vector<Position> GameManager::getAttackMovesForPawn(shared_ptr<Piece> pieceInFocus, string pieceSide, bool isKingPiece , bool checkForEnPassantMove) {

	// init and set fields
	vector<Position> possibleMovePositions;

	Pawn& currentPawn = dynamic_cast<Pawn&>(*pieceInFocus);

	// check if pawn is able to perform an enPassant move
	if (enPassantPawn != nullptr && checkForEnPassantMove) {

		int xPositionDifference = currentPawn.getPosition().getXPosition() - enPassantPawn->getPosition().getXPosition();
		if (currentPawn.getPosition().getYPosition() == enPassantPawn->getPosition().getYPosition() && abs(xPositionDifference) == 1) {

			Position enPassantPosition(enPassantPawn->getPosition().getXPosition(), currentPawn.getPosition().getYPosition() + currentPawn.getYMoveOffsets()[0]);
			possibleMovePositions.insert(possibleMovePositions.begin(), enPassantPosition);
		}

	}
		
	//calc attacking positions
	for (int i = 0; i < currentPawn.getAttackCoordsX().size(); i++) {
		int attackPosX = currentPawn.getPosition().getXPosition() +  currentPawn.getAttackCoordsX()[i];
		int attackPosY = currentPawn.getPosition().getYPosition() + currentPawn.getAttackCoordsY()[i];
		bool withingBoardRange = attackPosX < BOARD_DIMENSION && attackPosX >= 0 && attackPosY < BOARD_DIMENSION && attackPosY >= 0;
		if (withingBoardRange && (boardReference[attackPosX][attackPosY].find(getOpponentSide(pieceSide)) != std::string::npos || isKingPiece)) {
			Position newPosition(attackPosX, attackPosY);
			possibleMovePositions.insert(possibleMovePositions.begin(), newPosition);
		}
	}

	return possibleMovePositions;
}

/*
* return possible moves for a piece
*
* @param piece
*
* @return possibleMovePositions
*/
vector<Position> GameManager::computePossibleMoves(shared_ptr<Piece> piece) {
	//init fields
	vector<Position> possibleMovePositions;
	string opponentSide = BLACK_SIDE;

	// determine which set the piece is from
	if (piece -> getPieceSide() == BLACK_SIDE)
		opponentSide = WHITE_SIDE;

	// if the current piece is a King Piece
	if (piece->getPieceName().find(KING) != std::string::npos) return getPossibleMovesForKingPiece(piece, opponentSide);

	// else for a normal piece
	return removeUnallowableMoves(piece, getPossibleMovesForPiece(piece, opponentSide));
}

/*
* King is the only piece that cannot move to a position that is already underattack.
* Check possible King moves if any exist and remove any positions that are currently underattack.
*
* @param piece
* @param opponentSide
*
* @return allowedMoves: collection of allowed moves for a king piece
*/	
vector<Position> GameManager::getPossibleMovesForKingPiece(shared_ptr<Piece> piece, string opponentSide) {

	// set opponentSetPiece to blackSetPieces by Default , switch to whiteSetPieces if otherwise
	auto opponentSetPieces = blackSetPieces;
	if (opponentSide == WHITE_SIDE) opponentSetPieces = whiteSetPieces;

	// get all moves under attack by opponent , including positions that are protected by other pieces
	vector<Position> attackedPositions;
	vector<Position> opponentAttackedPositions;
	for (auto opponentPiece : opponentSetPieces) {

		// detect if there is a pawn available , hence king cannot move to a tile attacked by pawn
		if (opponentPiece->getPieceName().find(PAWN) != std::string::npos) {
			opponentAttackedPositions = getAttackMovesForPawn(opponentPiece, opponentPiece->getPieceSide(), true , false);
			attackedPositions.insert(attackedPositions.end(), opponentAttackedPositions.begin(), opponentAttackedPositions.end());
		}
		else {
			opponentAttackedPositions = getPossibleMovesForPiece(opponentPiece , getOpponentSide(opponentPiece ->getPieceName()) );
			attackedPositions.insert(attackedPositions.end(), opponentAttackedPositions.begin(), opponentAttackedPositions.end());
			vector<Position> guardedPositions = getGuardedPositionsForPiece(opponentPiece);

			// determine if there are any guarded positions for the current piece , if so add the position
			if (guardedPositions.size() > 0) attackedPositions.insert(attackedPositions.end(), guardedPositions.begin(), guardedPositions.end());
		}

	}

	// get possible moves for king and remove any positions that are currently under attack by the opponentAttackedPositions
	auto possibleKingMoves = getPossibleMovesForPiece(piece, getOpponentSide(piece -> getPieceSide()));
	vector<Position> allowedMoves;
	for (Position move : possibleKingMoves) {
		bool posAttacked = false;
		for (Position attackedPosition : attackedPositions) {
			if (attackedPosition.getXPosition() == move.getXPosition() && attackedPosition.getYPosition() == move.getYPosition()){
				posAttacked = true;
				break;
			}
		}
		if (!posAttacked) allowedMoves.insert(allowedMoves.begin(), move);
	}

	// check if king can castle , add the moves to king moves if possible
	King king = dynamic_cast<King&>(*piece);
	vector<Position> castlingMoves = getKingCastlingMoves(king, attackedPositions);
	if (castlingMoves.size() > 0) allowedMoves.insert(allowedMoves.end(), castlingMoves.begin(), castlingMoves.end());
		
	return allowedMoves;
}


/*
* Check if king is allowed to castle
*
* @param king
* @param attackedPositions : attackedPositions by opponent side
*
* @return castlingMoves : collection of possible castling moves
*/
vector<Position> GameManager::getKingCastlingMoves(King king, vector<Position> attackedPositions){
	
	vector<Position> castlingMoves;

	// if king has lost his castling rights , return empty vector
	if (!king.getCastlingRight()) return {};

	// set allySetPieces to whiteSet by default, switch if otherwise
	vector<shared_ptr<Piece>> allySetPieces = whiteSetPieces;
	if (king.getPieceSide() == BLACK_SIDE) allySetPieces = blackSetPieces;

	// flags to check which side is king permitted to castle, if any exist
	bool kingSideCastleValid  = true, queenSideCastleValid = true;

	// iterate over attackedPositions and check that the necessary tiles are not underattack and a castling move is eligible
	for (Position attackedPosition : attackedPositions) {

		if (attackedPosition.getXPosition() == king.getPosition().getXPosition() && attackedPosition.getYPosition() == king.getPosition().getYPosition())
			return {};

		// check for king side castle if it is eligible
		for (int i = 0; i < king.getKingSideCastleTilesX().size(); i++) {
				
			// check tiles between king and rook are empty and newPosition of king is not under attack or the tiles he is passing by
			int kingSideX = king.getPosition().getXPosition() + king.getKingSideCastleTilesX()[i];
			int kingSideY = king.getPosition().getYPosition() + king.getKingSideCastleTilesY()[i];
			bool kingSideEmptyTiles = boardReference[kingSideX][kingSideY] == EMPTY;

			if (! kingSideEmptyTiles || (attackedPosition.getXPosition() == kingSideX && attackedPosition.getYPosition() == kingSideY)) kingSideCastleValid = false;
		}
		//
		for (int i = 0; i < king.getQueenSideCastleTilesX().size(); i++) {
			int queenSideX = king.getPosition().getXPosition() + king.getQueenSideCastleTilesX()[i];
			int queenSideY = king.getPosition().getYPosition() + king.getQueenSideCastleTilesY()[i];
			bool queenSideEmptyTiles = boardReference[queenSideX][queenSideY] == EMPTY;
			if (!queenSideEmptyTiles || ((attackedPosition.getXPosition() == queenSideX && attackedPosition.getYPosition() == queenSideY) && i != 2))
				queenSideCastleValid = false;
		}

	}
	
	// king side castling check
	for (shared_ptr<Piece> currentPiece : allySetPieces) {
		if (currentPiece->getPieceName().find(ROOK) != std::string::npos) {
			Rook& rook = dynamic_cast<Rook&>(*currentPiece);

			if (!rook.isEligibleForCastling()) continue;
				
			if (rook.isOnKingSide() && kingSideCastleValid && rook.isEligibleForCastling()) 
				castlingMoves.insert(castlingMoves.begin(), king.getKingSideCastlePosition());
				
			if (!rook.isOnKingSide() && queenSideCastleValid && rook.isEligibleForCastling()) 
				castlingMoves.insert(castlingMoves.begin(), king.getQueenSideCastlePosition());
		}
	}
	
	return castlingMoves;
}

/*
* King is unable to capture pieces that are guarded by other opponent pieces , this function ensures that
*
* @param currentPiece : calculate if this piece is guarded or not
*
* @return guardedPositions : collection of guarded positions
*/
vector<Position> GameManager::getGuardedPositionsForPiece(shared_ptr<Piece> currentPiece) {

	Piece piece = *currentPiece;
	vector<Position> guardedPositions;
	string opponentSide = getOpponentSide(piece.getPieceSide());

	// iterate over piece offsets
	for (int i = 0; i < piece.getXMoveOffsets().size(); i++) {

		int x = 0;
		int newX = piece.getPosition().getXPosition() + piece.getXMoveOffsets()[i] + (piece.getXMoveOffsets()[i] * x);
		int newY = piece.getPosition().getYPosition() + piece.getYMoveOffsets()[i] + (piece.getYMoveOffsets()[i] * x);
		bool intersectedOpponentKing = false;

		while (newX < BOARD_DIMENSION && newY < BOARD_DIMENSION && newX >= 0 && newY >= 0) {
			// if piece movement collides with another ally piece
			if (boardReference[newX][newY].substr(0, 5).compare(piece.getPieceSide()) == 0) {
				guardedPositions.insert(guardedPositions.begin(), Position(newX, newY));
				break;
			}

			if (piece.isMovementLimited()) break;

			// this will ensure that the opponent king can not move to a illegal tile if in check , by extending the attack of unlimited movement pieces pass the collision 
			if (intersectedOpponentKing && boardReference[newX][newY].compare(EMPTY) == 0) {
				guardedPositions.insert(guardedPositions.begin(), Position(newX, newY));
				break;
			}

			// detect if the king is in check and flag it
			if (boardReference[newX][newY].compare(opponentSide + KING) == 0) intersectedOpponentKing = true;
			else if (boardReference[newX][newY].substr(0, 5).compare(opponentSide) == 0) break;

			//update values
			newX = piece.getPosition().getXPosition() + piece.getXMoveOffsets()[i] + (piece.getXMoveOffsets()[i] * x);
			newY = piece.getPosition().getYPosition() + piece.getYMoveOffsets()[i] + (piece.getYMoveOffsets()[i] * x);
			x++;
		}
	}

	return guardedPositions;
}

/* 
* returns the king piece position
*
* @param pieceSide : white or black king
*
* @return position of king
*/
Position* GameManager::getKingPosition(string pieceSide) {

	// set default set to white set , change if otherwise
	auto setInFocus = whiteSetPieces;
	if (pieceSide == BLACK_SIDE) setInFocus = blackSetPieces;

	for (auto piece : setInFocus)
		if (piece->getPieceName().compare(pieceSide + KING) == 0) return new Position(piece->getPosition().getXPosition(), piece->getPosition().getYPosition());

	return NULL;
}

/* 
* certain moves are not allowed as they put their king underattack , this function ensures that this is never the case
*
* @param piece
* @param possibleMoves : collection of moves that are possible to be made by the provided piece
*/
vector<Position> GameManager::removeUnallowableMoves(shared_ptr<Piece> piece, vector<Position> possiblePieceMoves) {

	//shared_ptr<Piece> piece_copy = piece;
	shared_ptr<Piece> piece_copy = make_shared<Piece>(*piece);
	vector<Position> acceptableMoves;

	// determine the opponent, i.e black or white
	string opponentSide = getOpponentSide(piece_copy->getPieceSide());
	vector<shared_ptr<Piece>> opponentSetPieces = blackSetPieces;

	if (opponentSide == WHITE_SIDE) opponentSetPieces = whiteSetPieces;

	Position originalPiecePosition = piece_copy->getPosition();
	Position* kingPos = getKingPosition(piece_copy->getPieceSide());

	int kingPosX = kingPos->getXPosition();
	int kingPosY = kingPos->getYPosition();


	string capturedPieceName;
	int capturedPiecePosX;
	int capturedPiecePosY;

	for (auto move : possiblePieceMoves) {

		bool opponentPieceCaptured = false;

		// check if move position is empty or contains an opponent piece , if so simulate capturing
		if (boardReference[move.getXPosition()][move.getYPosition()] != EMPTY) {
			capturedPieceName = boardReference[move.getXPosition()][move.getYPosition()];
			capturedPiecePosX = move.getXPosition();
			capturedPiecePosY = move.getYPosition();
			opponentPieceCaptured = true;
		}

		// update board by simulating movement
		boardReference[piece_copy->getPosition().getXPosition()][piece_copy->getPosition().getYPosition()] = EMPTY;
		boardReference[move.getXPosition()][move.getYPosition()] = piece_copy->getPieceName();

		bool checkThreatExists = false;
		for (auto opponentPiece : opponentSetPieces) {

			if (opponentPieceCaptured && opponentPiece->getPieceName() == capturedPieceName &&
				opponentPiece->getPosition().getXPosition() == capturedPiecePosX && opponentPiece->getPosition().getYPosition() == capturedPiecePosY)
				continue;

			for (auto possibleAttackPosition : getPossibleMovesForPiece(opponentPiece , getOpponentSide(opponentPiece ->getPieceSide())))
				if (possibleAttackPosition.getXPosition() == kingPosX && possibleAttackPosition.getYPosition() == kingPosY) checkThreatExists = true;
		}

		// if move does not put ally king under attack , allow it
		if (!checkThreatExists) acceptableMoves.insert(acceptableMoves.begin(), move);

		// reset board to its default
		boardReference[move.getXPosition()][move.getYPosition()] = EMPTY;

	}
	return acceptableMoves;
}


