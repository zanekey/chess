
#include "Piece.h"


class Knight : public Piece {
public:
	// init knight piece
	Knight(Position initPosition , string side) : Piece(initPosition , side) {
		
		pieceName = side + KNIGHT;
		imgFile = KNIGHT_PIECE_IMG_FILE;
		Piece::setSpriteSettings();
		limitedMovement = true;

		xMoveOffsets = { 1 , 2 , 2 , 1 , -1 , -2 , -2 , -1 };
		yMoveOffsets = { 2 , 1 , -1 , -2 , -2 , -1 , 1 , 2 };

	}
};