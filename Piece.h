#pragma once
#include "Position.h"
#include <list>
#include "SFML/Graphics.hpp"
#include "DEFINITIONS.h"
#include <iostream>

using namespace sf;

class Piece {

protected:
	Position position;
	Sprite sprite;
	Texture texture;
	string side;
	string imgFile;
	vector<int> xMoveOffsets;
	vector<int> yMoveOffsets;
	bool limitedMovement = NULL;
	string pieceName;

	Piece(Position initPosition , string side) : position(initPosition) , side(side) { 
		if (side != WHITE_SIDE || side != BLACK_SIDE)
			return;
	}

public:
	void setSpriteSettings() {
		texture.loadFromFile(PIECE_DIR + side + imgFile);
		texture.setSmooth(true);
		sprite.setTexture(texture);
		sprite.setScale(SPRITE_SCALE, SPRITE_SCALE);
		sprite.setPosition((float)position.getXSpritePosition(), (float)position.getYSpritePosition());
	}

	Sprite getSprite() { return sprite; }
	Position getPosition() { return position; }
	virtual void setPosition(Position newPosition) { position = newPosition; setSpritePosition((float) newPosition.getXSpritePosition(), (float) newPosition.getYSpritePosition()); }
	void setSpritePosition(int x, int y) { sprite.setPosition((float) x, (float)y); }
	string getPieceName() { return pieceName; }
	string getPieceSide() { return side;  }
	vector<int> getXMoveOffsets() { return xMoveOffsets; }
	vector<int> getYMoveOffsets() { return yMoveOffsets; }
	bool  isMovementLimited() { return limitedMovement;  }




};




