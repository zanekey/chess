#include "Piece.h"
#include "Position.h"

class Rook : public Piece {
private:
	bool eligibleForCastling;
	bool onKingSide = true;

public:
	Rook(Position initPosition, string side) : Piece(initPosition , side) {

		eligibleForCastling = true;

		pieceName = side + ROOK;
		imgFile = ROOK_PIECE_IMG_FILE;
		Piece::setSpriteSettings();
		limitedMovement = false;

		xMoveOffsets = { 0 , 1 , 0 , -1 };
		yMoveOffsets = { 1 , 0 , -1 , 0 };

		if (initPosition.getXPosition() == 0) onKingSide = false;
	}

	void setPosition(Position position){
		if (eligibleForCastling) eligibleForCastling = false;
		Piece::setPosition(position);
	}

	bool isOnKingSide() { return onKingSide; }
	bool isEligibleForCastling() { return eligibleForCastling;  }
};