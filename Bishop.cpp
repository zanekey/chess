#include "Piece.h"

class Bishop : public Piece {
	
public:
	Bishop(Position initPosition, string side) : Piece(initPosition , side) {
		
		imgFile = BISHOP_PIECE_IMG_FILE;
		Piece::setSpriteSettings();

		pieceName = side + BISHOP;
		limitedMovement = false;

		xMoveOffsets = { 1 , 1 , -1 , -1 };
		yMoveOffsets = { 1, -1 , -1 , 1 };
	}
};